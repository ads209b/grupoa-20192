package br.com.joaquimnabuco.ti2.grupoa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrupoaApplication {

    public static void main(String[] args) {
        SpringApplication.run(GrupoaApplication.class, args);
    }

}
